## ----------------------------------------------------------------------
## Makefile : makefile for debian-science-policy
## ----------------------------------------------------------------------

## ----------------------------------------------------------------------
## Document definitions
doc_name	:= debian-science-policy
doc_html 	:= $(doc_name).html
doc_xml 	:= $(doc_name).xml
doc_pdf		:= $(doc_name).pdf
doc_txt		:= $(doc_name).txt
doc_html_xsl	:= $(doc_html).xsl
doc_html_params	:= -m $(doc_html_xsl)
#this needed cause xmlto txt does not include section numbering by default
doc_txt_params := --stringparam section.autolabel=1 --stringparam section.label.includes.component.label=1

## ----------------------------------------------------------------------
# this can and will be overriden by a higher level makefile
PUBLISHDIR := alioth.debian.org:/home/groups/debian-science/htdocs/

# There is no difference between letter and a4, but a2 for instance works
PAPERSIZE  := a4

## ----------------------------------------------------------------------
## Targets
all:		html

validate: $(doc_xml)
	xmllint --valid --noout $(doc_xml)

html: $(doc_html)

$(doc_html): $(doc_xml) $(doc_html_xsl)
	xmlto $(doc_html_params) xhtml-nochunks $(doc_xml)

txt $(doc_txt): $(doc_xml)
	xmlto $(doc_txt_params) txt $(doc_xml)

pdf $(doc_pdf): $(doc_xml)
	xmlto --with-dblatex pdf $(doc_xml)


publish: 	html
		rsync -a $(doc_html) $(PUBLISHDIR)

clean:
	rm -rf $(doc_html) $(doc_txt) $(doc_pdf)

distclean:
	make clean
	find . -name "*~" -exec rm -rf {} \;

.PHONY: all publish clean distclean validate
